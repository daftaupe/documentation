# Cache

Fine-tuning the cache is key to good performance, especially if you have a big instance and/or want to
get the most out of your hardware. The following recommendations may be specific to some situations and
no solution fitting everyone is given.

## Leveraging Nginx Caching

!> **Warning:** this section is experimental and subject to change. Don't copy commands without adapting them to your situation first.

If you are using Nginx, you might want to benefit from its caching capabilities. Of course PeerTube does
application caching itself, but it's nowhere as efficient as a reverse proxy like Nginx when it comes to
static assets (and videos are a static asset, so we have some easy improvement there!). From the [documentation
of Nginx](https://www.nginx.com/blog/nginx-caching-guide/) (which we recommend you read for an in-depth
explanation of what follows):

> A content cache sits in between a client and an “origin server”, and saves copies of all the content it
> sees. If a client requests content that the cache has stored, it returns the content directly without
> contacting the origin server. This improves performance as the content cache is closer to the client,
> and more efficiently uses the application servers because they don’t have to do the work of generating
> pages from scratch each time.

Using the `proxy_cache` directive from Nginx, we can optimize the static route `/static/webseed` that we
already proxy in our configuration.

```nginx
  location /static/webseed {
    # comment this line out:
    # alias /var/www/peertube/storage/videos;

    # add these lines:
    proxy_pass http://localhost:1234;
    proxy_cache peertube_videos;
  }
```

And at the end of the file, outside any server blocks, add this:

```nginx
proxy_cache_path /var/cache/peertube/videos/
    levels=1:2 keys_zone=peertube_videos:10m
    max_size=15g inactive=7d use_temp_path=off;
proxy_cache_valid 200 60m;

server {
  listen localhost:1234;
  location /static/webseed {
    alias /var/www/peertube/storage/videos;
  }
}
```
