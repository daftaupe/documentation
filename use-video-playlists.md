# Video playlists

Much like what you might expect on other video platforms, a playlist on PeerTube is an ordered collection of videos that can be shared or kept private. You can freely add to a playlist your own videos, or videos from others.

You actually already have a private playlist associated with your account: your "Watch later" playlist!

## Creating a playlist

Creating new playlists can be done in the `My Library` > `Playlist` tab, or directly via the left menu. Then just click "Create a new playlist".

![List of a user's playlists](/assets/video-playlist-list.png)

Notice that a playlist can be associated with no channel, or any channel of your choice. However, when making a public playlist, it has to be associated with a channel.

You can choose a thumbnail for the playlist, as well as a title and a description. They will be the first thing seen when users see when doing a search.

![Form to create a playlist](/assets/video-playlist-creation.png)

## Adding videos to playlists

Quick actions appearing on miniatures on hover help you add videos quickly to your playlists.

![Quickly adding a video to playlists](/assets/video-playlist-quick-action.png)

## Ordering a playlist

Playlists are ordered, so that viewers can watch videos sequentially. It is up to you to order videos in your playlist once you have gathered them. Click the "Edit" button in the list of playlists to access a playlist drag and drop ordering list.

![Ordering a playlist](/assets/video-playlist-ordering.png)

## Watching a playlist

Viewing a playlist triggers a special mode of the video player: the videos of the current playlist are listed in a pane on the right of the player, to quickly navigate among them, and see the upcoming videos.

![Watching a playlist](/assets/video-playlist-watching.png)

